import React, { Component } from 'react'

class AddPerson extends Component {
  render() {
    return (
      <div>
        <form onSubmit={this.props.addPerson}>
          <input
            placeholder="Pablo"
            value={this.props.currentItem.name}
            onChange={this.props.handleInputName}
          />
          <input
            placeholder="100"
            value={this.props.currentItem.money}
            onChange={this.props.handleInputMoney}
          />
          <button type="submit">Agregar persona</button>
        </form>
      </div>
    )
  }
}

export default AddPerson
