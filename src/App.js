import React, { Component } from 'react'
import PersonPaid from './PersonPaid'
import AddPerson from './AddPerson'
import {sum, flatten, map, props} from 'ramda'

class App extends Component {
  state = {
    people: [],
    currentItem: {
      name: '',
      money: null,
    },
  }

  promedio = () => this.total() === 0 ? 0 : this.total() / this.cantidad()

  total = () => (
    sum(flatten(map((person) => props(['money'], person), this.state.people)))
  )

  cantidad = () => this.state.people.length

  addPerson = e => {
    e.preventDefault()
    const newItem = this.state.currentItem
    if (newItem.name !== '') {
      const items = [...this.state.people, newItem]
      this.setState({ people: items })
    }
    this.setState({ currentItem: {name: '', money: null} })
  }

  handleInputName = e => {
    this.setState({ currentItem: {name: e.target.value, money: this.state.currentItem.money }})
  }

  handleInputMoney= e => {
    this.setState({ currentItem: {money: e.target.value, name: this.state.currentItem.name}})
  }

  render() {
    return (
      <div>
        <div>
        Total: { this.total() }
        <br />
        cantidad: { this.cantidad() }
        <br />
        Promedio: { this.promedio() }
        </div>
      <div>
        {this.state.people.map((item, index) => (
          <p>
            <PersonPaid name={item.name} pagar={this.promedio()} puso={item.money} />
          </p>
        ))}
        <AddPerson
          addPerson={this.addPerson}
          handleInputName={this.handleInputName}
          handleInputMoney={this.handleInputMoney}
          currentItem={this.state.currentItem}
        />
        </div>
      </div>
    )
  }
}

export default App
